var program = require("commander");
var names = require("./names");
program.parse(process.argv);

program.args.forEach(function(name){
  names.delete(name);
});
