var program = require("commander");
var names = require("./names");
pkg = require("../package.json");
program.version(pkg.version)
  .command("add [name]", "add name to the collection.")
  .command("rename [from] [to]", "rename names on the collection.")
  .command("delete [name]", "delete name on the collection.")
  .command("list", "list names on the collection.");

program.on("--help", function() {
  console.log("Examples:");
  console.log("");
  console.log(" $ " + "cli" + " add Dima");
  console.log(" $ " + "cli" + " rename Dima Dmitry");
  console.log(" $ " + "cli" + " delete Vitaly");
  console.log(" $ " + "cli" + " list");
});

program.parse(process.argv);

if (process.argv.length === 2) {
  program.help();
}
