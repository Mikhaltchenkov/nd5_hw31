var program = require("commander");
var names = require("./names");
program.parse(process.argv);

[from, to, ...other] = program.args;
names.rename(from, to);
