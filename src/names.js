var mongodb = require('mongodb');
var mc = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/nd5_hw31';

exports.add = function(name) {
  if (name && typeof name  ===  'string') {
    mc.connect(url, (err, db) => {
      if (err) {
        throw new Error('Can\'t connect to MongoDB. Error:', err);
      } else {
        db.collection('names').insert({'name':name}, (err, result) => {
          db.close();
          if (err) {
            throw new Error('Error: ', err);
          } else {
            console.log('[add]:', name, 'OK.');
          }
        });
      }
    });
  } else {
      throw new Error('no name defined to add!');
  }
};

exports.rename = function(from, to) {
  if (from && typeof from  ===  'string' && to && typeof to  ===  'string') {
    mc.connect(url, (err, db) => {
      if (err) {
        throw new Error('Can\'t connect to MongoDB. Error:', err);
      } else {
        db.collection('names').update({'name':from}, {'name':to});
        console.log('[rename]:', from, to, 'OK');
        db.close();
      }
    });
  } else {
      throw new Error('no name defined to add!');
  }
};

exports.list = function() {
  mc.connect(url, (err, db) => {
    if (err) {
      throw new Error('Can\'t connect to MongoDB. Error:', err);
    } else {
      db.collection('names').find().toArray((err, result) => {
        db.close();
        if (err) {
          throw new Error('Error: ', err);
        } else {
          result.forEach(item => {
            console.log('[list]:', item.name);
          });
        }
      });
    }
  });
};

exports.delete = function(name) {
  mc.connect(url, (err, db) => {
    if (err) {
      throw new Error('Can\'t connect to MongoDB. Error:', err);
    } else {
      db.collection('names').remove({'name': name});
      db.close();
      console.log('[delete]:', name);
    }
  });
};
